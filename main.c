/*
4-Bit 16x2 LCD Example
Microcontroller systems laboratory works
Andrius Katkevicius
*/
//#define F_CPU 16000000UL   // Not necessary in simulator
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// Your global variables here

// Defination of pot and pin variables
#define LCD_Port         PORTD	// Define LCD Port (PORTA, PORTB, PORTC, PORTD)
#define LCD_DPin         DDRD	// Define 4-Bit Pins (PD4-PD7 at PORT D)
#define LCD_DATA0_PIN    PD4    // < pin for 4bit data bit 0  
#define LCD_DATA1_PIN    PD5    // < pin for 4bit data bit 1  
#define LCD_DATA2_PIN    PD6    // < pin for 4bit data bit 2  
#define LCD_DATA3_PIN    PD7    // < pin for 4bit data bit 3  
#define RSPIN 			 PD3	// RS Pin
#define ENPIN            PD2 	// E Pin
int runtime;			 //Timer for LCD

// LCD initialization function with the minimum commands list
void LCD_Init (void)
{
    // Data and control pins as outputs
    LCD_DPin |= (1<<LCD_DATA0_PIN)|(1<<LCD_DATA1_PIN)|(1<<LCD_DATA2_PIN)|(1<<LCD_DATA3_PIN)|(1<<RSPIN)|(1<<ENPIN);		//Control LCD Pins (D4-D7)
	_delay_ms(16);		//Wait more when 15 ms after start
  
  
	LCD_Action(0x02);	    // Returns the cursor to the home position (Address 0). Returns display to its original state if it was shifted. 
	LCD_Action(0x28);       // Data sent or received in 4 bit lengths (DB7-DB4)
	LCD_Action(0x0C);       // Display on, cursor off
	LCD_Action(0x06);       // Increment cursor (shift cursor to right)
	LCD_Action(0x01);       // Clean LCD
	_delay_ms(5);
}

// Function to send commands and data to the LCD
void LCD_Action( unsigned char cmnd )
{
	LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0);
	LCD_Port &= ~ (1<<RSPIN);
	LCD_Port |= (1<<ENPIN);
	_delay_us(1);
	LCD_Port &= ~ (1<<ENPIN);
	_delay_us(200);
	LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4);
	LCD_Port |= (1<<ENPIN);
	_delay_us(1);
	LCD_Port &= ~ (1<<ENPIN);
	_delay_ms(2);
}

// Clear whole LCD
void LCD_Clear()
{
	LCD_Action (0x01);		//Clear LCD
	_delay_ms(2);			//Wait to clean LCD
	LCD_Action (0x80);		//Move to Position Line 1, Position 1
}

// Prints string of chars 
void LCD_Print (char *str)
{
	int i;
	for(i=0; str[i]!=0; i++)
	{
		LCD_Port = (LCD_Port & 0x0F) | (str[i] & 0xF0);
		LCD_Port |= (1<<RSPIN);
		LCD_Port|= (1<<ENPIN);
		_delay_us(1);
		LCD_Port &= ~ (1<<ENPIN);
		_delay_us(200);
		LCD_Port = (LCD_Port & 0x0F) | (str[i] << 4);
		LCD_Port |= (1<<ENPIN);
		_delay_us(1);
		LCD_Port &= ~ (1<<ENPIN);
		_delay_ms(2);
	}
}

// Prints string of chars to the specific location
// row - the row 0 or 1;
// pos - the column from 0 till 16
void LCD_Printpos (char row, char pos, char *str)
{
	if (row == 0 && pos<16)
	LCD_Action((pos & 0x0F)|0x80);
	else if (row == 1 && pos<16)
	LCD_Action((pos & 0x0F)|0xC0);
	LCD_Print(str);
}

// Your functions here
volatile int k = 1;
volatile int gron = 1;
volatile int reon = 1;
volatile int yeon = 1;
volatile int pushed = 1;

int slide(char *str){
    char tmp = *(str+15);
      char tmptmp;
      int i = 14;
      while(i != -1){
          tmptmp = *(str+i);
          *(str+i)=tmp;
          tmp = tmptmp;
          i--;
      }
      *(str+15) = tmp;
}

ISR(PCINT0_vect){
 if(pushed == 1){
  if(!(PINB&(1<<0))){
    	k = (k+1)%3;
  }
  if(!(PINB&(1<<2))){
    if(k==0){
    	yeon*= -1;
    }
    else{
      if(k==1){
      	gron *= -1;
      }
      else{
      	reon *= -1;
      }
    }
  }
  if(!(PINB&(1<<1))){
    	k--;
    if (k==-1)
      k=2;
  }
  }
  pushed *=-1;
}

int main()
{
  	DDRB = 0x38;												
	LCD_Init();
  	PCICR |= (1<<PCIE0);
  	PCMSK0 |= ((1<<PCINT0)|(1<<PCINT1)|(1<<PCINT2));
  	sei();
  	//mandatory_part();
    additional_part();
}

int mandatory_part(void){
  char *str = "VilniusTech     ";
  LCD_Printpos(0, 0, str);
  while(1) {
      	slide(str);
		LCD_Printpos(1, 0, str);
      _delay_ms(100);
	}
}
int additional_part(void){
  char *str = "VilniusTech Menu";
  char *yon = "LED Yellow: On  ";
  char *yoff ="LED Yellow: Off ";
  char *ron = "LED Red: On     ";
  char *roff ="LED Red: Off    ";
  char *gon = "LED Green: On   ";
  char *goff ="LED Green: Off  ";
  LCD_Printpos(0,0,str);
  while(1){
    if(k==0){
      if(yeon == -1){
        LCD_Printpos(1,0,yon);
      }
      else{
        LCD_Printpos(1,0,yoff);
      }
    }
    else{
      if(k==1){
      	if(gron == -1){
        LCD_Printpos(1,0,gon);
      }
      else{
        LCD_Printpos(1,0,goff);
      }
      }
      else{
      	if(reon == -1){
        LCD_Printpos(1,0,ron);
      }
      else{
        LCD_Printpos(1,0,roff);
      }
      }
    }
    if(gron==-1){
      PORTB |= 1<<4;
    }
    else{
    	PORTB = PORTB&0b11101111;
    }
    if(reon==-1){
      PORTB |= 1<<3;
    }
    else{
    	PORTB = PORTB&0b11110111;
    }
    if(yeon==-1){
      PORTB |= 1<<5;
    }
    else{
    	PORTB = PORTB&0b11011111;
    }
  }
}